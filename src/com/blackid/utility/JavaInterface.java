package com.blackid.utility;



import com.blackidn.islamictimeline.InnerWebview;
import com.blackidn.islamictimeline.SelectTimelineActivity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.webkit.JavascriptInterface;
import android.widget.Toast;

public class JavaInterface {

	Context context;
	public JavaInterface(Context c)
	{
		context=c;	
	}
	
	/** Show a toast from the web page */
	@JavascriptInterface
    public void getFormatedPost(String urlid) {
        //Toast.makeText(mContext, urlid, Toast.LENGTH_SHORT).show();
		Intent i=new Intent(context, InnerWebview.class);
		i.putExtra("url", urlid);
		context.startActivity(i);
		//Toast.makeText(context, urlid, Toast.LENGTH_LONG).show();
    }
	
	public void goToHome(){
    	Intent i = new Intent(context,SelectTimelineActivity.class);
    	context.startActivity(i);
    	((Activity) context).finish();
    }
}
