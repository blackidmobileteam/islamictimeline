package com.blackidn.islamictimeline;

import java.io.IOException;
import java.util.ArrayList;

import org.apache.http.client.ClientProtocolException;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.blackid.islamincbean.TimelineObj;
import com.blackid.jsonparser.Jsonparser;
import com.blackid.utility.Constant;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.GridView;
import android.widget.TextView;

public class SelectTimelineActivity extends Activity{
	
	TextView christianView,islamicView;
	GridView selectTimeline;
	ProgressDialog progress_dialog;
	ArrayList<TimelineObj> timelienTypes;
	SelectTimelineAdapter adapter;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.choice_timeline_layout);
		
		selectTimeline 	= (GridView)findViewById(R.id.choice_timeline_view);
		progress_dialog	= new ProgressDialog(SelectTimelineActivity.this);
		
		progress_dialog.setMessage("Loading...");
		progress_dialog.setCancelable(true);
		progress_dialog.setCanceledOnTouchOutside(false);
		
		timelienTypes	= new ArrayList<TimelineObj>();
		new AsyctTimelineList().execute();
		
	}
	
	class AsyctTimelineList extends AsyncTask<Void, Void, Void>{
		
		Jsonparser jsonParser;
		String json;
		JSONObject jsonObj;
		
		@Override
		protected void onPreExecute() {
			// TODO Auto-generated method stub
			super.onPreExecute();
			jsonParser = new Jsonparser();
			progress_dialog.show();
		}

		@Override
		protected Void doInBackground(Void... params) {
			// TODO Auto-generated method stub
			String url = Constant.URL+"services/s.php?req=get_calender_type";
			Log.e("url",url);
			try {
				json 	= jsonParser.getJsonfromUrl(url, "post");
				jsonObj	= new JSONObject(json);
				
			} catch (ClientProtocolException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return null;
		}
		
		@Override
		protected void onPostExecute(Void result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			try {
				JSONArray timelineType = jsonObj.getJSONArray("timelines");
				for(int i=0;i<timelineType.length();i++){
					JSONObject timelineObj = timelineType.getJSONObject(i);
					int id 		= timelineObj.getInt("term_id");
					String name	= timelineObj.getString("name");
					String slug = timelineObj.getString("slug");
					
					TimelineObj obj = new TimelineObj(id,name,slug);
					timelienTypes.add(obj);
				}
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			progress_dialog.dismiss();
			adapter = new SelectTimelineAdapter(SelectTimelineActivity.this,timelienTypes);
			selectTimeline.setAdapter(adapter);
			
			selectTimeline.setOnItemClickListener(new OnItemClickListener() {

				@Override
				public void onItemClick(AdapterView<?> arg0, View arg1,
						int position, long arg3) {
					// TODO Auto-generated method stub
					Intent i = new Intent(SelectTimelineActivity.this,MainTimeLineView.class);
					i.putExtra("timeline_type", timelienTypes.get(position).getTimelineSlug());
					startActivity(i);
				}
			});
			
		}

	}
}
