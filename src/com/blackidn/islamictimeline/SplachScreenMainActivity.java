package com.blackidn.islamictimeline;

import com.blackid.preferences.PreferenceConnector;

import android.os.Bundle;
import android.os.Handler;
import android.app.Activity;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup.LayoutParams;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.ImageView;
import android.widget.ProgressBar;

public class SplachScreenMainActivity extends Activity {

	
	private int progressBarStatus = 0;


	private Handler progressBarHandler = new Handler();
	ProgressBar progress_bar;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		setContentView(R.layout.splach_screen_main_activity);
		
		// removing status bar in
		//getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,WindowManager.LayoutParams.FLAG_FULLSCREEN);

		// reset progress bar status
		
		String checked=PreferenceConnector.readString(this,PreferenceConnector.DIALOG_CHECKED, "no");
		
		if(checked.equalsIgnoreCase("no"))
		{
			showSettingAlertDialog();
			
		}
		else
		{
			startProgress();
		}

	}
	
	public void startProgress()
	{
		progressBarStatus = 0;
		
		progress_bar = (ProgressBar) findViewById(R.id.progressBar1);
		
		Drawable customDrawable= getResources().getDrawable(R.drawable.custom_progressbar);
		progress_bar.setProgressDrawable(customDrawable); 
		progress_bar.setProgress(0);
		
		new Thread(new Runnable() {
			public void run() {
				while (progressBarStatus < 100) {

					// process some tasks
					progressBarStatus++;

					// your computer is too fast, sleep 1 second
					try {
						Thread.sleep(25);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}

					// Update the progress bar
					progressBarHandler.post(new Runnable() {
						public void run() {
							progress_bar.setProgress(progressBarStatus);
						}
					});
				}
				
				// ok, file is downloaded,
				if (progressBarStatus >= 20) {

					Intent mainIntent = new Intent(SplachScreenMainActivity.this,SelectTimelineActivity.class);
					SplachScreenMainActivity.this.startActivity(mainIntent);
					SplachScreenMainActivity.this.finish();
				}
			}
		}).start();
	}
	
	public void showSettingAlertDialog()
	{
		// custom warning dialog
		final Dialog dialog = new Dialog(SplachScreenMainActivity.this,android.R.style.Theme_Translucent_NoTitleBar);
		dialog.requestWindowFeature(Window.FEATURE_LEFT_ICON);
		dialog.setContentView(R.layout.warning_dialog);
		//dialog.setFeatureDrawableResource(Window.FEATURE_LEFT_ICON,R.drawable.yellow_warning_icon);
		dialog.getWindow().setLayout(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);
		//dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
		//dialog.setTitle("Warnning");
		dialog.setCancelable(true);
		dialog.setCanceledOnTouchOutside(false);
		
		dialog.setOnCancelListener(new OnCancelListener() {
			
			@Override
			public void onCancel(DialogInterface dialog) {
				// TODO Auto-generated method stub
				SplachScreenMainActivity.this.finish();
			}
		});
		
		final CheckBox check=(CheckBox)dialog.findViewById(R.id.checkBox_donotdisplay);
				
		ImageView btn_dismiss=(ImageView)dialog.findViewById(R.id.button_dismiss);
		
		btn_dismiss.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if(check.isChecked())
				{
					PreferenceConnector.writeString(SplachScreenMainActivity.this, PreferenceConnector.DIALOG_CHECKED,"yes");
				}
				else
				{
					PreferenceConnector.writeString(SplachScreenMainActivity.this, PreferenceConnector.DIALOG_CHECKED,"no");
				}
				dialog.dismiss();
				startProgress();
			}
		});
					
		dialog.show();
		
	}
	
	/*@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		// TODO Auto-generated method stub
		if (keyCode == KeyEvent.KEYCODE_BACK) {
			 moveTaskToBack(true);
		}
		return super.onKeyDown(keyCode, event);
		
	}*/

	
}