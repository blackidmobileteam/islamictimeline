package com.blackidn.islamictimeline;


import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpVersion;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.CoreProtocolPNames;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;

import com.blackid.utility.ConnectionDetector;
import com.blackid.utility.Constant;
import com.blackid.utility.JavaInterface;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.webkit.WebSettings.ZoomDensity;
import android.widget.FrameLayout;

public class MainTimeLineView extends Activity{

	
	ConnectionDetector connection_detector;
	WebView webview_timeline;
	String timeline_type;
	ProgressDialog progress_dialog;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main_time_line_view_screen);
		
		connection_detector=new ConnectionDetector(this);
		Intent i = getIntent();
		timeline_type = i.getStringExtra("timeline_type");
		webview_timeline=(WebView)findViewById(R.id.webView_time_line);
		webview_timeline.setBackgroundColor(0x00000000);
		webview_timeline.clearCache(true);
		webview_timeline.getSettings().setUseWideViewPort(true);
		webview_timeline.setHorizontalScrollBarEnabled(true); 
		webview_timeline.setVerticalScrollBarEnabled(true);
		webview_timeline.getSettings().setBuiltInZoomControls(false);
	    webview_timeline.getSettings().setSupportZoom(false);
		webview_timeline.getSettings().setBuiltInZoomControls(false);
		progress_dialog=new ProgressDialog(this);
		
		progress_dialog.setMessage("Loading...");
		progress_dialog.setCancelable(true);
		progress_dialog.setCanceledOnTouchOutside(false);
		progress_dialog.setOnCancelListener(new OnCancelListener() {
			
			@Override
			public void onCancel(DialogInterface dialog) {
				// TODO Auto-generated method stub
				MainTimeLineView.this.finish();
			}
		});
		
		if(connection_detector.isConnectingToInternet())
		{
			new AsyncTimeline(this).execute();
			//webview_timeline.getSettings().setJavaScriptEnabled(true);
			//webview_timeline.getSettings().setPluginsEnabled(true);
		        
			/*webview_timeline.setClickable(false);
			webview_timeline.setHorizontalScrollBarEnabled(true); 
			webview_timeline.setVerticalScrollBarEnabled(true); 
			webview_timeline.getSettings().setDefaultZoom(ZoomDensity.FAR);
			webview_timeline.getSettings().setSupportZoom(true);
			webview_timeline.getSettings().setBuiltInZoomControls(true);
		        
			webview_timeline.getSettings().setUseWideViewPort(true);*/
			//webview_timeline.loadUrl("http://demo.blackidlabs.com/timeline/mobile-timeline/");
			/*webview_timeline.setWebViewClient(new WebViewClient() {
				
				   public void onPageFinished(WebView view, String url) {
					   
				      progress_dialog.dismiss();
					}
				   
			});*/
			if(progress_dialog.isShowing())
			{
				webview_timeline.setWebViewClient(new WebViewClient() {
					
					public void onPageFinished(WebView view, String url) {
							   
						progress_dialog.dismiss();
					}
				});
				
			}
			
		}
		else
		{
			progress_dialog.dismiss();
			connection_detector.showSettingAlertDialog();
		}
		
	}
	
	@Override
	public void onPause() {
	    super.onPause();

	   /* if(progress_dialog != null)
	    		progress_dialog.dismiss();*/
	    
	    //progress_dialog = null;
	}
	
	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
	/*	if(progress_dialog != null)
    		progress_dialog.show();
    		*/
	}
	
	Handler goto_Home = new Handler() {

		@Override
		public void handleMessage(Message msg) {
		
			super.handleMessage(msg);
			
		}
		
		
	};
	
	class AsyncTimeline extends AsyncTask<Void, Void, Void>
	{
		

		Context context;
		public AsyncTimeline(Context context)
		{
			this.context=context;
			progress_dialog.show();
		}
		
		
		@Override
		protected Void doInBackground(Void... params) {
			// TODO Auto-generated method stub
			
			runOnUiThread(new Runnable() {
				
				@Override
				public void run() {
					
					// TODO Auto-generated method stub
					webview_timeline.getSettings().setJavaScriptEnabled(true);
					webview_timeline.addJavascriptInterface(new JavaInterface(context), "myAndroid");
					Log.e("url_original",Constant.URL+"mobile-timeline/?ctype="+timeline_type);
					webview_timeline.loadUrl(Constant.URL+"mobile-timeline/?ctype="+timeline_type);
				}
			});
			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			
		}

		
		
	}
	
}
