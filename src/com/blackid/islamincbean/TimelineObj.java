package com.blackid.islamincbean;

public class TimelineObj {
	int timeline_id;
	String timelineName;
	String timelineSlug;
	
	public TimelineObj(){
		
	}
	
	public TimelineObj(int id,String name,String slug){
		timeline_id 	= id;
		timelineName 	= name;
		timelineSlug	= slug;
	}
	
	public void setTimelineId(int id){
		timeline_id = id;
	}
	
	public void setTimelineName(String name){
		timelineName = name;
	}
	
	public void setTimelineSlug(String slug){
		timelineSlug = slug;
	}
	
	public int getTimelineId(){
		return timeline_id;
	}
	
	public String getTimelineName(){
		return timelineName;
	}
	
	public String getTimelineSlug(){
		return timelineSlug;
	}
}
