package com.blackidn.islamictimeline;

import java.util.ArrayList;

import com.blackid.islamincbean.TimelineObj;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

public class SelectTimelineAdapter extends BaseAdapter{
	
	Context context;
	LayoutInflater inflater;
	TextView timelineName;
	ArrayList<TimelineObj> timelineList;
	
	public SelectTimelineAdapter(Context context,ArrayList<TimelineObj> timelineList) {
		// TODO Auto-generated constructor stub
		this.context		= context;
		inflater			= (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		this.timelineList	= timelineList;
	}
	
	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return timelineList.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return timelineList.get(position);
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		convertView 	= inflater.inflate(R.layout.grid_view_item, parent, false);
		timelineName	= (TextView) convertView.findViewById(R.id.timeline_name);
		timelineName.setText(timelineList.get(position).getTimelineName());
		return convertView;
	}

}
