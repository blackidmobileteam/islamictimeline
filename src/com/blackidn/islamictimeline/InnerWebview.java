package com.blackidn.islamictimeline;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

import com.blackid.utility.ConnectionDetector;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.webkit.WebView;
import android.webkit.WebViewClient;

public class InnerWebview extends Activity{

	String url;
	ConnectionDetector connection_detector;
	WebView webview_timeline_iiner;
	ProgressDialog progress_dialog;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.inner_webview_screen);
		url=getIntent().getStringExtra("url");
		Log.e("URL",url);
		webview_timeline_iiner=(WebView)findViewById(R.id.webView_inner);
		connection_detector=new ConnectionDetector(this);
		progress_dialog=new ProgressDialog(this);
		progress_dialog.setMessage("Loading...");
		progress_dialog.setCancelable(true);
		progress_dialog.setCanceledOnTouchOutside(false);
		progress_dialog.setOnCancelListener(new OnCancelListener() {
			
			@Override
			public void onCancel(DialogInterface dialog) {
				// TODO Auto-generated method stub
				InnerWebview.this.finish();
			}
		});
		
		if(connection_detector.isConnectingToInternet())
		{
			new AsyncTimelineInner(this).execute();
			//webview_timeline.getSettings().setJavaScriptEnabled(true);
			//webview_timeline.getSettings().setPluginsEnabled(true);
		        
			/*webview_timeline.setClickable(false);
			webview_timeline.setHorizontalScrollBarEnabled(true); 
			webview_timeline.setVerticalScrollBarEnabled(true); 
			webview_timeline.getSettings().setDefaultZoom(ZoomDensity.FAR);
			webview_timeline.getSettings().setSupportZoom(true);
			webview_timeline.getSettings().setBuiltInZoomControls(true);
		        
			webview_timeline.getSettings().setUseWideViewPort(true);*/
			//webview_timeline.loadUrl("http://demo.blackidlabs.com/timeline/mobile-timeline/");
			/*webview_timeline.setWebViewClient(new WebViewClient() {
				
				   public void onPageFinished(WebView view, String url) {
					   
				      progress_dialog.dismiss();
					}
				   
			});*/
			if(progress_dialog.isShowing())
			{
				webview_timeline_iiner.setWebViewClient(new WebViewClient() {
					
					public void onPageFinished(WebView view, String url) {
							   
						progress_dialog.dismiss();
					}
				});
				
			}
			
		}
		else
		{
			progress_dialog.dismiss();
			connection_detector.showSettingAlertDialog();
		}
		
	}
	@Override
	public void onPause() {
		    super.onPause();
	
		    if(progress_dialog != null)
		    		progress_dialog.dismiss();
		    
		    //progress_dialog = null;
	}
	
	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		if(progress_dialog != null)
    		progress_dialog.show();
	}
	
	 
	
	class AsyncTimelineInner extends AsyncTask<Void, Void, Void>
	{
		

		Context context;
		public AsyncTimelineInner(Context context)
		{
			this.context=context;
			progress_dialog.show();
		}
		
		
		@Override
		protected Void doInBackground(Void... params) {
			// TODO Auto-generated method stub
			runOnUiThread(new Runnable() {
				
				@Override
				public void run() {
					// TODO Auto-generated method stub
					
					webview_timeline_iiner.getSettings().setJavaScriptEnabled(true);
					//webview_timeline_iiner.addJavascriptInterface(new JavaInterface(context), "myAndroid");
					webview_timeline_iiner.clearCache(true);
					webview_timeline_iiner.loadUrl(url);
				}
			});
			return null;
		}

		@Override
		protected void onPostExecute(Void result) {
			// TODO Auto-generated method stub
			super.onPostExecute(result);
			
		}

		
		
	}
	
	

}
